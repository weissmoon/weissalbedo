package weissmoon.albedo.tile;

import elucent.albedo.event.ProfilerStartEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by Weissmoon on 11/27/17.
 */
public class FurnaceGlowing{

    @SubscribeEvent
    public void onProfileStart(ProfilerStartEvent evnt){
        if (evnt.getSection().compareTo("terrain") == 0) {
            World world = Minecraft.getMinecraft().world;
            if (world == null) return;

            for (TileEntity t : world.loadedTileEntityList) {
                if (t instanceof TileEntityFurnace) {
                    if (world.getBlockState(t.getPos()).getBlock().equals(Block.getBlockById(62)))
                        LightManager.addLight(new Light.Builder().pos(t.getPos()).color(1F, 0.5F, 0F, 1F).radius(7).build());
                    //LightManager.addLight(new Light.Builder().pos(t.getPos()).color(17F, 10F, 7F, 0.06F).radius(7).build());
                }
            }
        }
    }
}
