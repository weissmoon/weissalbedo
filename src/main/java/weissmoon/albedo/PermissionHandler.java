package weissmoon.albedo;

import weissmoon.albedo.WeissAlbedo;
import weissmoon.albedo.network.PlayerPermisionPacket;

/**
 * Created by Weissmoon on 5/31/18.
 */
public class PermissionHandler {

    public static void handleServerPermisionPacket(PlayerPermisionPacket packet){
        boolean[] perms = new boolean[3];
        perms[0] = packet.players;
        perms[1] = packet.items;
        perms[2] = packet.world;
        WeissAlbedo.instance.handleSeverConnect(perms);
    }
}
