package weissmoon.albedo.compat;

import lumien.hardcoredarkness.HardcoreDarkness;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.*;
import weissmoon.albedo.world.WorldLightUpper;

/**
 * Created by Weissmoon on 3/2/18.
 */
public class HardcoreDarknessCompat implements CompatModule {

    private WorldLightUpper upper;

    public boolean isDimensionBlacklist(int id){
        return HardcoreDarkness.INSTANCE.getActiveConfig().isDimensionBlacklisted(id);
    }

    @Override
    public void preInit(FMLPreInitializationEvent event) {
        upper = new WorldLightUpper();
        MinecraftForge.EVENT_BUS.register(upper);
    }

    @Override
    public void init(FMLInitializationEvent event) {

    }

    @Override
    public void postInit(FMLPostInitializationEvent event) {

    }

    @Override
    public void serverAbouttoStart(FMLServerAboutToStartEvent evt) {

    }

    @Override
    public void serverStarting(FMLServerStartingEvent evt) {

    }

    @Override
    public void serverStarted(FMLServerStartedEvent evt) {

    }

    @Override
    public void serverAbouttoStop(FMLServerStoppingEvent evt) {

    }

    @Override
    public void serverStoped(FMLServerStartingEvent evt) {

    }

    public String getModName() {
        return "hardcoredarkness";
    }
}
