package weissmoon.albedo.compat;

import net.minecraftforge.fml.common.event.*;

/**
 * Created by Weissmoon on 12/30/17.
 */
public interface CompatModule {
    public void preInit (FMLPreInitializationEvent event);
    public void init (FMLInitializationEvent event);
    public void postInit (FMLPostInitializationEvent event);
    public void serverAbouttoStart (FMLServerAboutToStartEvent evt);
    public void serverStarting (FMLServerStartingEvent evt);
    public void serverStarted (FMLServerStartedEvent evt);
    public void serverAbouttoStop (FMLServerStoppingEvent evt);
    public void serverStoped (FMLServerStartingEvent evt);

    public String getModName();
}
