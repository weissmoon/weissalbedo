package weissmoon.albedo.server.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Created by Weissmoon on 1/15/18.
 */
public class PlayerPermisionPacket implements IMessage {

    public boolean items;
    public boolean players;
    public boolean world;

    @Override
    public void fromBytes(ByteBuf buf) {
        this.players = buf.readBoolean();
        this.items = buf.readBoolean();
        this.world = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.players);
        buf.writeBoolean(this.items);
        buf.writeBoolean(this.world);
    }

    public static class Handler implements IMessageHandler<PlayerPermisionPacket, IMessage> {

        @Override
        public IMessage onMessage(PlayerPermisionPacket message, MessageContext ctx) {
            //NO-OP server
            return null;
        }
    }
}
