package weissmoon.albedo.api;

import elucent.albedo.lighting.Light;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

/**
 * Created by Weissmoon on 2/5/18.
 * Partial Light copy to allow coloring of lights.
 */
public class LightColor {
    public float r;
    public float g;
    public float b;
    public float a;
    public float radius;

    public LightColor(float r, float g, float b, float a, float radius){
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
        this.radius = radius;
    }

    public static Builder builder(LightColor color) {
        return new Builder(color);
    }

    public static final class Builder {
        private float x = Float.NaN;
        private float y = Float.NaN;
        private float z = Float.NaN;

        public float r;
        public float g;
        public float b;
        public float a;
        public float radius;


        public Builder(LightColor color) {
            this.r = color.r;
            this.g = color.g;
            this.b = color.b;
            this.a = color.a;
            this.radius = color.radius;

        }


        public Builder pos(BlockPos pos) {
            return pos(pos.getX()+0.5f, pos.getY()+0.5f, pos.getZ()+0.5f);
        }

        public Builder pos(Vec3d pos) {
            return pos(pos.x, pos.y, pos.z);
        }

        public Builder pos(Entity e) {
            return pos(e.posX, e.posY, e.posZ);
        }

        public Builder pos(double x, double y, double z) {
            return pos((float)x, (float)y, (float)z);
        }

        public Builder pos(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
            return this;
        }

        public Light build() {
            if (Float.isFinite(x) && Float.isFinite(y) && Float.isFinite(z) &&
                    Float.isFinite(r) && Float.isFinite(g) && Float.isFinite(b) && Float.isFinite(a) &&
                    Float.isFinite(radius)) {
                return new Light(x, y, z, r, g, b, a, radius);
            } else {
                throw new IllegalArgumentException("Position, color, and radius must be set, and cannot be infinite");
            }
        }

    }
}
