package weissmoon.albedo.entity;

import elucent.albedo.event.RenderEntityEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by Weissmoon on 12/2/17.
 */
@Mod.EventBusSubscriber
public class CreeperCharged {

    @SubscribeEvent
    public void renderCharged(RenderEntityEvent evnt){
        Entity entity = evnt.getEntity();
        if(entity instanceof EntityCreeper){
            if(((EntityCreeper) entity).getPowered()){
                LightManager.addLight(new Light.Builder()
                        .pos(entity.getEntityBoundingBox().getCenter())
                        .color( 0.8f, 0.8f, 0.8f, 1.5F)
                        .radius(3)
                        .build());
            }
        }
    }
}
