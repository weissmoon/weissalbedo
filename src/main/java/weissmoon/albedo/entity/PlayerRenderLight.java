package weissmoon.albedo.entity;

import elucent.albedo.event.RenderEntityEvent;
import elucent.albedo.lighting.LightManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.client.event.RenderSpecificHandEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.albedo.Lists;
import weissmoon.albedo.api.IItemLightProvider;
import weissmoon.albedo.api.LightColor;
import weissmoon.albedo.api.WeissAlbedoAPI;

/**
 * Created by Weissmoon on 1/12/18.
 */
public class PlayerRenderLight {

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void renderPlayer(RenderEntityEvent evnt){
        if (evnt.getEntity() instanceof EntityPlayer){
            EntityPlayer player = (EntityPlayer)evnt.getEntity();
            if(!player.isInvisibleToPlayer(Minecraft.getMinecraft().player)){
                ItemStack stack;
                for(EnumHand hand:EnumHand.values()){
                    stack = player.getHeldItem(hand);
                    if(stack.getItem() instanceof IItemLightProvider){
                        LightManager.addLight(LightColor.builder(
                                ((IItemLightProvider) stack.getItem()).provideLight(stack, player, hand)).
                                pos(player.getEntityBoundingBox().getCenter()).
                                build());
                    }else if(WeissAlbedoAPI.EntityItemProvider.containsKey(stack.getItem())){
                        IItemLightProvider provider = WeissAlbedoAPI.EntityItemProvider.get(stack.getItem());

                        LightManager.addLight(LightColor.builder(
                                provider.provideLight(stack, player, hand)).
                                pos(player.getEntityBoundingBox().getCenter()).
                                build());
                    }else if(light(stack)) {
                        LightManager.addLight(LightColor.builder(Lists.PlayerItemList.get(stack.getItem())).
                                pos(player.getEntityBoundingBox().getCenter()).
                                build());
//                        LightManager.addLight(new Light.Builder().
//                                pos(player.getEntityBoundingBox().getCenter()).
//                                color(1F, 1F, 1F).
//                                radius(5F).
//                                build());
                    }
                }
            }
        }
    }

    @SubscribeEvent(priority=EventPriority.LOWEST)
    public void renderHand(RenderSpecificHandEvent event){
        ItemStack stack = Minecraft.getMinecraft().player.getHeldItem(event.getHand());
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        if(!(stack == ItemStack.EMPTY)){
            if(stack.getItem() instanceof IItemLightProvider){
                LightManager.addLight(LightColor.builder(
                        ((IItemLightProvider) stack.getItem()).provideLight(stack, player, event.getHand())).
                        pos(player.getEntityBoundingBox().getCenter()).
                        build());
            }else if(WeissAlbedoAPI.PlayerItemProvider.containsKey(stack.getItem())){
                IItemLightProvider provider = WeissAlbedoAPI.PlayerItemProvider.get(stack.getItem());

                LightManager.addLight(LightColor.builder(
                        provider.provideLight(stack, player, event.getHand())).
                        pos(player.getEntityBoundingBox().getCenter()).
                        build());
            }else if(light(stack)) {
                LightManager.addLight(LightColor.builder(Lists.PlayerItemList.get(stack.getItem())).
                        pos(player.getEntityBoundingBox().getCenter()).
                        build());
//                        LightManager.addLight(new Light.Builder().
//                                pos(player.getEntityBoundingBox().getCenter()).
//                                color(1F, 1F, 1F).
//                                radius(5F).
//                                build());
            }
        }
    }


    private boolean light(ItemStack stack){
        for(Item item:Lists.PlayerItemList.keySet()){
            ItemStack stack1 = new ItemStack(item);
            if(stack1.isItemEqualIgnoreDurability(stack)){
                return true;
            }else if(stack1.isItemEqual(stack)){
                return true;
            }
        }
        return false;
    }

}
