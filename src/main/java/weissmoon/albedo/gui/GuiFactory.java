package weissmoon.albedo.gui;

import com.google.common.collect.ImmutableSet;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.config.ConfigGuiType;
import net.minecraftforge.fml.client.config.DummyConfigElement;
import net.minecraftforge.fml.client.config.IConfigElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class GuiFactory implements IModGuiFactory{
    public void initialize (Minecraft minecraft){
    }

    @Override
    public boolean hasConfigGui() {
        return true;
    }



    @Override
    public GuiScreen createConfigGui(GuiScreen parentScreen) {
        return new ModGuiConfig(parentScreen);
    }

    private static final Set<RuntimeOptionCategoryElement> fmlCategories = ImmutableSet.of(new RuntimeOptionCategoryElement("general", "FML"));

    public Set<RuntimeOptionCategoryElement> runtimeGuiCategories (){
        return fmlCategories;
    }

//    public RuntimeOptionGuiHandler getHandlerFor (RuntimeOptionCategoryElement element){
//        return null;
//    }
}
